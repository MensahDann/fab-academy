---
title: "Home"
date: 2021-01-27T14:29:25+02:00
draft: false
layout: "home"
---

# Welcome to my Fab Academy Documentation site!

On this site I will be documenting projects I have made during Aalto University's Digital Fabrication courses, which are run in tandem with [Fab Academy](https://fabacademy.org/). Click the links on the sidebar to navigate. Enjoy your stay!

