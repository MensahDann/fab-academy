---
title: "Assignment 2: Principles and Practices"
date: 2021-02-13T08:32:44+02:00
draft: false
---
## Tasks:
- Read the Fab Charter.
- Sketched your final project idea(s).
- Described what it will do and who will use it.
- Added a final project page, image(s) and description to your documentation website.
- Added a link to the final project section on your website.

## Submission:
- Fab Charter read ✅
- [Final Project page added]({{< baseurl >}}final)✅
- [Final Project idea sketch]({{< baseurl >}}final/v0)✅