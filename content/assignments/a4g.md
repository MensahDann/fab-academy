---
title: "Assignment 4 Computer-Controlled Cutting (Group): "
date: 2021-02-15T08:32:44+02:00
draft: false
---
## Tasks:
- characterize your lasercutter's focus, power, speed, rate, kerf, and joint clearance
- document your work (individually or in group)

## Submission:

### Group Members: 吳若玄 (Ruo-Xuan Wu), Daniel Wilenius, Dann Mensah

### Focus

Laser cutters have a lens in them, which focuses the laser's energy on a focus spot.
If an object is placed too far away from the focus spot, the energy from the laser is
spread on a much larger area, and it might not be able to penetrate the material. 
The Epilog Legend 36EXT laser cutter that we used has two ways of focusing the laser's lens: manual focus and auto focus.

To manually focus the laser, attached a small calibrating object to the laser. When the bottom of the object touches
the material on the cutting bed, we know that the distance from the lens to the material is exactly the same as the focal
length of the lens. Once the height of the cutting bed is adjusted properly, the object is removed, and we can proceed with the cutting.

The other option is auto focus, which is operated using the laser's driver software on the attached PC. To use it, we simply ticked a box 
on the software, and once we initiated the cutting process, the cutter would spend around 10 seconds to adjust the bed's height to the proper position.
We found that using this setting on the first piece we cut, and then ticking it off for subsequent pieces was the easiest and fastest way to make sure 
that the laser was focused.

### Power, Speed and Rate

The driver software also gave us three parameters we could adjust in order to control the cutting process: power, speed and rate.
Power is essentially how much energy the laser is focusing on the focus spot. The higher the power, the more energy we are cutting with, and vice versa.
We found that adjusting the power had a substantial impact on the result. We tried power settings from 20 to 100 with increments of 20, with a speed of 20.

{{< a4gimg src=images/a4g/power_squares.jpg text="Different power settings. P for power, S for Speed" >}}

While the lowest settings cut the plywood squares out with minimal waste, the higher power settings left a large gap between the cut out square and the 
surrounding material. This essentially means that with higher power we lost more material from the square we designed.


{{< a4gimg src=images/a4g/p20size.jpg text="Square cut with power 20. Designed length 30mm" >}}
{{< a4gimg src=images/a4g/p100size.jpg text="Square cut with power 100. Designed length 30mm" >}}

The speed parameter controls how fast the laser is moving over the material. When the speed is higher, the laser will spend less time in any one spot, 
and thus will burn less material in that spot. Unsurprisingly, we found a similar effect of material loss with different speeds as we did with power, except
that the relationship was inverted. With lower speeds we lost more material and vice versa. We tried speed settings from 20 to 100 with increments of 20, with a power of 20.
Out of our experiments, the laser was able to penetrate the plywood only with the lowest speed setting.

{{< a4gimg src=images/a4g/speed_squares.jpg text="Different speed settings. P for power, S for Speed" >}}

The rate parameter of the laser cutter adjusts how many times per unit of distance the laser "pulses" on. With a very low rate you would end up with a sequence of holes instead of a continuous line.
To our understanding this parameter is usually chosen per material and is adjusted less empirically than power or speed.


{{< a4gimg src=images/a4g/settings_squares.jpg text="All of our parameter experiments. P for power, S for Speed" >}}

{{< a4gvid src=videos/video.mp4 text="Engraving and cutting processes" >}}


### Kerf
The kerf of a laser cutter is the amount of material it burns away as it cuts. We designed five pieces of 30mm x 30mm and cut them using the recommended
settings for 2mm plywood (power: 38, speed: 60, rate: 500). We then measured the widths of both sides of each piece and averaged them. We subtracted the result
from 30mm, and as a result got about 0.22mm, which is the kerf. This means, that whenever we design a vector along which we cut, the actual thickness of that line is 0.22mm.
In order to avoid problems, this has to be taken into account and rectified during the design process.

An interesting finding was that horizontal and vertical cuts made by the laser cutter seemed to have different kerfs. We measured a kerf of .29 and .15 for vertical and horizontal cuts, respectively. It is worth noting that
this difference is quite significant compared to the averaged kerf.
Our hypothesis is that for whatever reason the focus spot of the laser isn't exactly circular. This could happen, for example, when the material that is being cut is at an angle with respect to the laser, resulting in an oval shaped focus spot.

### Joint Clearance

The joint clearance is the space designed between two surfaces that form a joint. This adjustment is made on top of whatever corrections is needed to account for the kerf. We did two joint clearance related experiments.

For the first experiment we designed pairs of [press-fit joints](http://academy.cba.mit.edu/classes/computer_cutting/joints.png) with different joint clearances.

{{< a4gimg src=images/a4g/joints.jpg text="An example of a press-fit joint" >}}

We started by only accounting for the kerf of the laser, which means that we added 0.2mm to the designed joint gap of 2mm. Surprisingly, this resulted in a joint that was too loose, which shouldn't be the case. Our hypothesis is that this is caused
by the difference in the kerfs of horizontal and vertical cuts, as well as the variations in material thickness. We started decreasing the joint gap 0.1mm at a time, until at a gap of 1.9mm we found that the joint was tight.

{{< a4gimg src=images/a4g/all_pressfits.jpg text="Tests with different joint clearances for the press-fit joint. Numbers are the differences from 2mm (reported material thickness)" >}}

{{< a4gimg src=images/a4g/pressfits.jpg text="Final tight press-fit joint with a gap of 1.9mm" >}}

For our second experiment we designed a classic finger-joint box. 


{{< a4gimg src=images/a4g/joint_vectors.jpg text="Design for finger-joint box" >}}

We started with only the kerf compensation in the design and no joint clearance. We found the resulting joints to be too tight, so iteratively we increased the joint clearance, until at a clearance of 0.1mm the fit was perfect.

{{< a4gimg src=images/a4g/box_closed.jpg text="Finger-joint box with 0.1mm joint clearance" >}}
{{< a4gimg src=images/a4g/box_open.jpg text="Finger-joint box with 0.1mm joint clearance" >}}