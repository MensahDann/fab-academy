---
title: "Assignment 6: Scanning and Printing"
date: 2021-03-16T08:32:44+02:00
draft: false
---

## Scanning

We did the 3D scanning part together with Eero. Both our work can be seen [Here](https://eeropic.gitlab.io/fablab-doc/assignments/assignment-06-scan).

## Printing

For my printing project I modeled a small vase that is difficult to manufacture subtractively as it curves inside. I made it as parametric as possible so that I could easily change the shape and size.
{{< a4gimg src=images/a6/vase.jpg text="3D Model of the vase" >}}
{{< a4gimg src=images/a6/sectional_analysis.jpg text="Sectional analysis of the vase" >}}
{{< a4gimg src=images/a6/variables.jpg text="Vase parameters" >}}
After designing the vase, I exported the .stl and imported it in Ultimaker Cura. Most of the things that were new to me happened in this phase. I learned the process for changing the filament of the Ultimaker 2, and Solomon explained to me how the base plate is calibrated (manual vs. automatic in some other printers). I checked the preview of the print with different support settings and printing resolutions. I went with the normal preset and didn't add any supports.
{{< a4gimg src=images/a6/print_settings2.png text="Ultimaker Cura Settings" >}}
{{< a4gimg src=images/a6/print_settings.png text="Ultimaker Cura Settings" >}}
I then saved the .gcode file to the SD card and inserted it into the printer. After that I simply waited for the print to finish.
Around mid-way through the printing process I had the idea of putting something inside the vase that would be too large to get out later. This is definitely something that subtractive methods don't enable.
{{< a4gimg src=images/a6/IMG_20210302_111643.jpg text="Coin inside the vase" >}}
{{< a4gimg src=images/a6/IMG_20210302_111647.jpg text="Almost done" >}}
And then I was done!
{{< a4gimg src=images/a6/IMG_20210314_194455.jpg text="Finished vase!" >}}

